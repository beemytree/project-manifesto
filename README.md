# README #

This README provides reader with a full description of Bee My Tree project concept.

Bee My Tree logo at : <https://drive.google.com/drive/folders/1JWitKGMZ3kuWUY2d99yquHIr6lF04Gk9>

## INDEX ##
- Presenting Bee My Tree project
- The problems we face
- How can Bee My Tree solve these problems?

## Presenting Bee My Tree project ##

*In the context of innovative technologies, how can people use them to stand for a cause?*

This is one of the reasons why Nexid Edge has started a campaign to promote and support companies in the transition to **eco-sustainability**. 
Years ago, a massive project was brought to life in a way to support companies to control energy consumption of their buildings and minimize it. At the present time, thanks to **artificial intelligence** and **IoT**, more than 5000 buildings abroad in Italy are consuming less energy for -at least- the same business goals.

**Now**, Nexid Edge wants to enlarge the pool of instruments available to contrast climate changes and pollution by proposing and developing a totally new tool: **Bee My Tree**.
Bee My Tree concept is to **compensate** part of the **environmental impact** that italian (and in future we hope world) companies cause on ecosystems by planting new **trees** and supporting beekeepers in building and maintaining new **hives**.

*Every modification of the environment will be respectful of the existent ecosystem and its ecological balance.  Ecological balance has been defined by various online dictionaries as "a state of dynamic equilibrium within a community of organisms in which genetic, species and ecosystem diversity remain relatively stable, subject to gradual changes through natural succession." and "A stable balance in the numbers of each species in an ecosystem."*

This project will be built using blockchain and IoT technologies. Bees and trees adoption will be certified by a NFT representing the specifical tree or hive. At the same time, the new forests and hives will be monitored by an IoT sensor system that will be installed nearby the interested zones. All the data coming from these sensors will be automatically synchronized with blockchain updating NFT data. 

Both mobile and web apps will be made available to companies in order to adopt trees and hives and certify them via NFT and control the health status of their forests/bees. Companies can also value their adoptions by showing them on social media.
In order to plant new trees, build new hives and take care of both trees and bees, partnership with beekeepers, farmers and lumberjacks will be needed. Nexid Edge is also interested in acquisition of disused farmlands to re-convert them into hoods in northern Italy.

Blockchain hasn’t been known for being an eco-sustainable technology, until PoS algorithms weren’t seriously adopted by blockchain developers. In this framework, Nexid Edge strongly believes that Algorand would represent a valid and solid partner for these reasons:

- Both Algorand and Nexid Edge shares eco-sustainability values;
- Algorand is a truly **secure** and **transparent decentralized ecosystem**;
- **Scalability**: Algorand transaction rate is about 1000 TPS;
- Easy **app** implementation thanks to a solid set of libraries;
- Intuitive **NFT ecosystem**;
- User friendly **wallet** application.

At the same time, Nexid Edge is proposing Algorand its project since it promises to become a new tool to raise eco-sustainability awareness in business and, in future project stages, incentivize it with benefits such as branded honey and credits representing the level of companies’ involvement in eco-sustainability race. The long term aim is to give companies the tools to convert their adoptions into assets which can be used in their sustainability report.

## The problems we face ##
Pollution, deforestation, excessively high emissions’ levels are unfortunately few examples of the issues that humankind are actually facing. No more than a few years ago, it was a natural process that companies continued to look forward to their progress despite its eco-sustainability. 

Today, the approach to business solutions is changing and sustainability is gaining considerable importance during all the parts of the processes involved. At the same time, customers too are adopting a more green mindset asking companies for more transparency with regards to environmental involvement of their business.
This mindset shows its effects on marketing campaigns and green products/services that companies are, now more than ever, proposing to their customers: cars, cosmetics and food are only scratching the surface of this new environmental cause sensitivity.

There already exist platforms that let you plant trees and adopt bees but there is no instrument that lets you measure your commitment and monitor your progress as a company.

So, how can we measure companies' commitment? A company can define their products or services as green, but how to demonstrate it? At the same time, how to incentivize this virtuous behaviour? 

And finally, we encounter another issue to manage: the solution to the problem must not be more harmful than the problem itself. We want to incentivize green behaviour but the means that we offer cannot worsen the situation.

That explains one of the reasons why Nexid Edge desires Bee My Tree to be supported and powered by Algorand blockchain to answer all the questions that have been raised in this section. Nexid Edge offers a technological and innovative tool which involves the adoption of a green blockchain technology to support a green aim. At the same time, the tool is expected to survive a long period of time since it can be economically sustainable due Algorand low transaction fees. 
Apart from shared core values, Nexid Edge desires to develop Bee My Tree project on Algorand blockchain thanks to Algorand’s technological strong suits too:

- **Proof Of Stake** consensus algorithm: Algorand is a truly secure and transparent decentralized ecosystem;
- **Scalability**: Algorand transaction rate is about 1000 TPS;
- **Easy app implementation** thanks to a solid set of libraries;
- Intuitive **NFT ecosystem**:
	- **Direct Mint**, no Smart Contract needed;
	- Possibility to set four strategic **management addresses**.
- User friendly wallet application.

## How can Bee My Tree solve these problems? ##
### Vision and solutions ###

Nexid Edge’s **goal** is to **create** an intuitive and user friendly **product** that paves the way to drive **companies** on their path to **green** transition.

It is a truth that every human activity on our planet leaves a footprint that can be easily transformed into pure exploitation of nature. Otherwise, mindset can be changed from *stealing* from nature to *cooperating* with nature.

When it comes to the point that companies desire more means to give back nature what they took, Bee My Tree offers the opportunity to commit to heal the environment. 

We strongly believe that part of the environmental problems that we have can be relieved by planting and protecting new trees and defending one of the most crucial animals on this planet: bees. Without any bee, it couldn’t be life. Bees pollinate a lot of the plants that are crucial to other animals and so, humans too, survival. 

It is important to remember that, every human modification to an existing ecosystem must respect it and so, every new tree will be planted in its natural habitat. The same stands for bees: the selected breed must be respectful of the existing ecosystem. We do not want to change the environmental balance, on the contrary we would be harmful, but we desire to respect and enhance it.

*Each plant or bee will, then, be appropriately evaluated since it is important to respect both the environment and pre-existing animals. Eco-system evaluation will be performed with help of our partners and experts (e.g. agronomists and entomologists).*

Bee My Tree will be an innovative tool since it will represent a true instrument to measure and certify companies’ eco-sustainability commitment. It is not our aim to substitute already existing platform such as Treedom ( <https://www.treedom.net/it/> ) or 3Bee ( <https://www.3bee.com/> ).
On the contrary, we are evaluating such platforms as possible partners. 

Our product will attend to the promises we are making since it will use a combination of IoT and blockchain technologies to measure and certify companies’ green commitment. Every tree or hive which will be adopted by companies will be certified on blockchain via NFT. At the same time an IoT sensor system will be installed nearby them to monitor and collect data. Every data will be automatically pushed on blockchain and associated with the NFT representing trees/hives.

Bee My Tree will be composed of both a mobile and a web application that allows customers to adopt hives and plant new trees. Our aim is to incentivize this positive behaviour. We do not want this to become a last chance resource, but a strong companies' ally to contrast pollution and nature exploitation. 
To do so, good behaviour will be incentivized since we would like to customize NFT purchased by companies with their brands, so they will be pushed to publish them on social channels and incentives other companies to do the same. 

Another incentive is that *nature gives back a gift* such as honey or fruit from the adopted bees and trees and the possibility to add a brand tag to them to present clients or employees with those benefits. 
Where it is possible we would like to install not only IoT sensors but also cameras. Companies will be able to watch their bees and trees and to receive updates about their health and their positive environmental impact. Data coming from sensors could be shared.

In future development we would like to build a system to actively measure companies' commitment and let companies use them to prove it in their sustainability balance.

### High level mechanism ###
In summary, our vision will be concretized in a purchase application system supported by Algorand blockchain to certify adoption and IoT measurements via NFT. Each company will be able to control, follow and publish on social media its trees and hives. 

The user will have the possibility to use both mobile and web applications as he/she prefers to purchase NFTs and monitor their adoptions. 
The graphic concept is reported in: <https://drive.google.com/drive/folders/10XURelg-kBH7XlWRowr5C5tVuM3imd80>

### Success  ###
A great success would be represented by an *exponential* effect in the adoption of the system. We hope that the companies that show their commitment on social channels will incentivize other companies to do the same and so on, enlarging the pool of users enormously. **Nexid Edge itself will devolve a percentage of their income to the cause, with the aim to drag its clients too.**

At the same time we believe that if the companies could prove their eco-sustainability commitment, their business would benefit from it too. Clients would be more likely to purchase the services and/or products offered by the virtuous companies and this would incentivize other companies to join the cause.

In future development, the asset certification for sustainability report and the benefit systems will represent the cherry on top of the incentive mechanism that we would like to spring from the adoption of our tool.

On the other hand, we know that blockchain is not, at a first sight, an easy to adopt instrument. People are used to thinking in a centralized manner and we would like to shift the focus on a decentralized way of doing things.

The initial blockchain barrier could cause a slowdown in the adoption of Bee My Tree application.

For this reason we are analyzing possible solutions to overcome the initial obstacle that users could encounter in adoption of blockchain. Under investigation are solutions to help people in adoption and management of their wallet. At the same time we are discussing the possible ways to get users familiar with cryptocurrencies in the most soft possible way.








